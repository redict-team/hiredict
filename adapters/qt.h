/*
 * Copyright (C) 2014 Pietro Cerutti <gahr@gahr.ch>
 *
 * SPDX-FileCopyrightText: 2024 Hiredict Contributors
 * SPDX-FileCopyrightText: 2024 Pietro Cerutti <gahr@gahr.ch>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-License-Identifier: LGPL-3.0-or-later
 *
 */

#ifndef __HIREDICT_QT_H__
#define __HIREDICT_QT_H__
#include <QSocketNotifier>
#include "../async.h"

static void RedictQtAddRead(void *);
static void RedictQtDelRead(void *);
static void RedictQtAddWrite(void *);
static void RedictQtDelWrite(void *);
static void RedictQtCleanup(void *);

class RedictQtAdapter : public QObject {

    Q_OBJECT

    friend
    void RedictQtAddRead(void * adapter) {
        RedictQtAdapter * a = static_cast<RedictQtAdapter *>(adapter);
        a->addRead();
    }

    friend
    void RedictQtDelRead(void * adapter) {
        RedictQtAdapter * a = static_cast<RedictQtAdapter *>(adapter);
        a->delRead();
    }

    friend
    void RedictQtAddWrite(void * adapter) {
        RedictQtAdapter * a = static_cast<RedictQtAdapter *>(adapter);
        a->addWrite();
    }

    friend
    void RedictQtDelWrite(void * adapter) {
        RedictQtAdapter * a = static_cast<RedictQtAdapter *>(adapter);
        a->delWrite();
    }

    friend
    void RedictQtCleanup(void * adapter) {
        RedictQtAdapter * a = static_cast<RedictQtAdapter *>(adapter);
        a->cleanup();
    }

    public:
        RedictQtAdapter(QObject * parent = 0)
            : QObject(parent), m_ctx(0), m_read(0), m_write(0) { }

        ~RedictQtAdapter() {
            if (m_ctx != 0) {
                m_ctx->ev.data = NULL;
            }
        }

        int setContext(redictAsyncContext * ac) {
            if (ac->ev.data != NULL) {
                return REDICT_ERR;
            }
            m_ctx = ac;
            m_ctx->ev.data = this;
            m_ctx->ev.addRead = RedictQtAddRead;
            m_ctx->ev.delRead = RedictQtDelRead;
            m_ctx->ev.addWrite = RedictQtAddWrite;
            m_ctx->ev.delWrite = RedictQtDelWrite;
            m_ctx->ev.cleanup = RedictQtCleanup;
            return REDICT_OK;
        }

    private:
        void addRead() {
            if (m_read) return;
            m_read = new QSocketNotifier(m_ctx->c.fd, QSocketNotifier::Read, 0);
            connect(m_read, SIGNAL(activated(int)), this, SLOT(read()));
        }

        void delRead() {
            if (!m_read) return;
            delete m_read;
            m_read = 0;
        }

        void addWrite() {
            if (m_write) return;
            m_write = new QSocketNotifier(m_ctx->c.fd, QSocketNotifier::Write, 0);
            connect(m_write, SIGNAL(activated(int)), this, SLOT(write()));
        }

        void delWrite() {
            if (!m_write) return;
            delete m_write;
            m_write = 0;
        }

        void cleanup() {
            delRead();
            delWrite();
        }

    private slots:
        void read() { redictAsyncHandleRead(m_ctx); }
        void write() { redictAsyncHandleWrite(m_ctx); }

    private:
        redictAsyncContext * m_ctx;
        QSocketNotifier * m_read;
        QSocketNotifier * m_write;
};

#endif /* !__HIREDICT_QT_H__ */
