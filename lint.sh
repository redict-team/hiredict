#!/bin/sh -e
# SPDX-FileCopyrightText: 2024 Hiredict Contributors
# SPDX-License-Identifier: LGPL-3.0-or-later

signoff() {
    if ! git log --format='%b' HEAD^.. | grep 'Signed-off-by' >/dev/null
    then
        echo "Missing sign-off!" >&2
        echo "Run 'git commit --amend -s' to fix" >&2
        echo "See CONTRIBUTING.md" >&2
        return 1
    fi
}

licenses() {
    if ! chronic sh -c "reuse lint 2>/dev/null"
    then
        return 1
    fi
}

exitcode=0
for step in signoff licenses
do
    if ! $step
    then
        exitcode=1
    fi
done

if [ $exitcode -eq 0 ]
then
    echo "Everything looks good!"
fi
exit $exitcode
