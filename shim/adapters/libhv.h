/*
 * Copyright (c) 2009-2011, Salvatore Sanfilippo <antirez at gmail dot com>
 *
 * SPDX-FileCopyrightText: 2024 Hiredict Contributors
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-License-Identifier: LGPL-3.0-or-later
 *
 */

#ifndef __HIREDIS_LIBHV_H__
#define __HIREDIS_LIBHV_H__

#include <hiredict/adapters/libhv.h>
#include <hiredis/hiredis.h>
#include <hiredis/async.h>

#define redisLibhvEvents redictLibhvEvents

#define redisLibhvHandleEvents redictLibhvHandleEvents
#define redisLibhvAddRead redictLibhvAddRead
#define redisLibhvDelRead redictLibhvDelRead
#define redisLibhvAddWrite redictLibhvAddWrite
#define redisLibhvDelWrite redictLibhvDelWrite
#define redisLibhvCleanup redictLibhvCleanup
#define redisLibhvTimeout redictLibhvTimeout
#define redisLibhvSetTimeout redictLibhvSetTimeout
#define redisLibhvAttach redictLibhvAttach

#endif
