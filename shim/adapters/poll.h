/*
 * Copyright (c) 2009-2011, Salvatore Sanfilippo <antirez at gmail dot com>
 *
 * SPDX-FileCopyrightText: 2024 Hiredict Contributors
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-License-Identifier: LGPL-3.0-or-later
 *
 */

#ifndef HIREDIS_POLL_H
#define HIREDIS_POLL_H

#include <hiredict/adapters/poll.h>
#include <hiredis/async.h>
#include <hiredis/sockcompat.h>

/* Values to return from redictPollTick */
#define REDIS_POLL_HANDLED_READ REDICT_POLL_HANDLED_READ
#define REDIS_POLL_HANDLED_WRITE REDICT_POLL_HANDLED_WRITE
#define REDIS_POLL_HANDLED_TIMEOUT REDICT_POLL_HANDLED_TIMEOUT

/* An adapter to allow manual polling of the async context by checking the state
 * of the underlying file descriptor.  Useful in cases where there is no formal
 * IO event loop but regular ticking can be used, such as in game engines. */
#define redisPollEvents redictPollEvents

#define redisPollTimevalToDouble redictPollTimevalToDouble
#define redisPollGetNow redictPollGetNow
#define redisPollTick redictPollTick
#define redisPollAddRead redictPollAddRead
#define redisPollDelRead redictPollDelRead
#define redisPollAddWrite redictPollAddWrite
#define redisPollDelWrite redictPollDelWrite
#define redisPollCleanup redictPollCleanup
#define redisPollAttach redictPollAttach

#endif /* HIREDIS_POLL_H */
