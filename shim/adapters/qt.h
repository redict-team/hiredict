/*
 * Copyright (C) 2014 Pietro Cerutti <gahr@gahr.ch>
 *
 * SPDX-FileCopyrightText: 2024 Hiredict Contributors
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-License-Identifier: LGPL-3.0-or-later
 *
 */

#ifndef __HIREDIS_QT_H__
#define __HIREDIS_QT_H__

#include <hiredict/adapters/qt.h>
#include <hiredis/async.h>

#define RedisQtAddRead RedictQtAddRead
#define RedisQtDelRead RedictQtDelRead
#define RedisQtAddWrite RedictQtAddWrite
#define RedisQtDelWrite RedictQtDelWrite
#define RedisQtCleanup RedictQtCleanup

#define RedisQtAdapter RedictQtAdapter

#endif /* !__HIREDIS_QT_H__ */
