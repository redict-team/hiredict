/*
 * SPDX-FileCopyrightText: 2024 Hiredict Contributors
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-License-Identifier: LGPL-3.0-or-later
 *
 */

#ifndef __HIREDIS_SSL_H
#define __HIREDIS_SSL_H

/* A wrapper around OpenSSL SSL_CTX to allow easy SSL use without directly
 * calling OpenSSL.
 */
#define redisSSLContext redictSSLContext

/**
#define redisCreateSSLContext redictCreateSSLContext
 */
#define redisSSLContextError redictSSLContextError
#define REDIS_SSL_CTX_NONE REDICT_SSL_CTX_NONE                                             /* No Error */
#define REDIS_SSL_CTX_CREATE_FAILED REDICT_SSL_CTX_CREATE_FAILED                           /* Failed to create OpenSSL SSL_CTX */
#define REDIS_SSL_CTX_CERT_KEY_REQUIRED REDICT_SSL_CTX_CERT_KEY_REQUIRED                   /* Client cert and key must both be specified or skipped */
#define REDIS_SSL_CTX_CA_CERT_LOAD_FAILED REDICT_SSL_CTX_CA_CERT_LOAD_FAILED               /* Failed to load CA Certificate or CA Path */
#define REDIS_SSL_CTX_CLIENT_CERT_LOAD_FAILED REDICT_SSL_CTX_CLIENT_CERT_LOAD_FAILED       /* Failed to load client certificate */
#define REDIS_SSL_CTX_CLIENT_DEFAULT_CERT_FAILED REDICT_SSL_CTX_CLIENT_DEFAULT_CERT_FAILED /* Failed to set client default certificate directory */
#define REDIS_SSL_CTX_PRIVATE_KEY_LOAD_FAILED REDICT_SSL_CTX_PRIVATE_KEY_LOAD_FAILED       /* Failed to load private key */
#define REDIS_SSL_CTX_OS_CERTSTORE_OPEN_FAILED REDICT_SSL_CTX_OS_CERTSTORE_OPEN_FAILED     /* Failed to open system certificate store */
#define REDIS_SSL_CTX_OS_CERT_ADD_FAILED REDICT_SSL_CTX_OS_CERT_ADD_FAILED                 /* Failed to add CA certificates obtained from system to the SSL context */ 

/* Constants that mirror OpenSSL's verify modes. By default,
#define redisCreateSSLContext redictCreateSSLContext
 * Some Redict clients disable peer verification if there are no
 * certificates specified.
 */
#define REDIS_SSL_VERIFY_NONE REDICT_SSL_VERIFY_NONE
#define REDIS_SSL_VERIFY_PEER REDICT_SSL_VERIFY_PEER
#define REDIS_SSL_VERIFY_FAIL_IF_NO_PEER_CERT REDICT_SSL_VERIFY_FAIL_IF_NO_PEER_CERT
#define REDIS_SSL_VERIFY_CLIENT_ONCE REDICT_SSL_VERIFY_CLIENT_ONCE
#define REDIS_SSL_VERIFY_POST_HANDSHAKE REDICT_SSL_VERIFY_POST_HANDSHAKE

/* Options to create an OpenSSL context. */
#define redisSSLOptions redictSSLOptions

/**
 * Return the error message corresponding with the specified error code.
 */
#define redisSSLContextGetError redictSSLContextGetError

/**
 * Helper function to initialize the OpenSSL library.
 *
 * OpenSSL requires one-time initialization before it can be used. Callers should
 * call this function only once, and only if OpenSSL is not directly initialized
 * elsewhere.
 */
#define redisInitOpenSSL redictInitOpenSSL

/**
 * Helper function to initialize an OpenSSL context that can be used
 * to initiate SSL connections.
 *
 * cacert_filename is an optional name of a CA certificate/bundle file to load
 * and use for validation.
 *
 * capath is an optional directory path where trusted CA certificate files are
 * stored in an OpenSSL-compatible structure.
 *
 * cert_filename and private_key_filename are optional names of a client side
 * certificate and private key files to use for authentication. They need to
 * be both specified or omitted.
 *
 * server_name is an optional and will be used as a server name indication
 * (SNI) TLS extension.
 *
 * If error is non-null, it will be populated in case the context creation fails
 * (returning a NULL).
 */
#define redisCreateSSLContext redictCreateSSLContext

/**
  * Helper function to initialize an OpenSSL context that can be used
#define redisCreateSSLContext redictCreateSSLContext
  *
  * options contains a structure of SSL options to use.
  *
  * If error is non-null, it will be populated in case the context creation fails
  * (returning a NULL).
*/
#define redisCreateSSLContextWithOptions redictCreateSSLContextWithOptions

/**
 * Free a previously created OpenSSL context.
 */
#define redisFreeSSLContext redictFreeSSLContext

/**
 * Initiate SSL on an existing redictContext.
 *
#define redisInitiateSSL redictInitiateSSL
 * to directly interact with OpenSSL, and instead uses a redictSSLContext
#define redisCreateSSLContext redictCreateSSLContext
 */
#define redisInitiateSSLWithContext redictInitiateSSLWithContext

/**
 * Initiate SSL/TLS negotiation on a provided OpenSSL SSL object.
 */
#define redisInitiateSSL redictInitiateSSL

#endif  /* __HIREDICT_SSL_H */
