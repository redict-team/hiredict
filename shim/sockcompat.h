/*
 * Copyright (c) 2019, Marcus Geelnard <m at bitsnbites dot eu>
 *
 * SPDX-FileCopyrightText: 2024 Hiredict Contributors
 * SPDX-FileCopyrightText: 2024 Marcus Geelnard <m at bitsnbites dot eu>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-License-Identifier: LGPL-3.0-or-later
 *
 */

#ifndef __HIREDIS_SOCKCOMPAT_H
#define __HIREDIS_SOCKCOMPAT_H

#include <hiredict/sockcompat.h>

#endif /* __HIREDIS_SOCKCOMPAT_H */
